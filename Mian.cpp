//基于车牌颜色的车牌识别part1，车牌的定位与透视变换					//
//  因为车牌颜色在不同环境下影响较大，所以需要根据环境修改蓝色的范围//
// 开发环境:opencv2.4.13.2(OpenCV2)									//
// 字符定位暂时是设置得固定点，后面采取字符分割，要处理汉字被分为多个部分的问题

#include<opencv2/opencv.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<string.h>
#include<iostream>
using namespace cv;
using namespace std;
void ex(IplImage*);
cv::Point2f getPoint(cv::Vec4i a, cv::Vec4i b);
void sortCorners(std::vector<cv::Point2f>& corners, cv::Point2f center);//顶点排序
IplImage* pic_Thresholding(IplImage *);
int detectionChange(Mat& mat1, Mat&mat2, int number, Mat& mat3, Mat& mat4);
void verProjection_calculate(Mat&mat, int* vArry, int number);
int **verProjection_cut(int* vArr, int width, int*number);
float pixelPerectange(Mat& mat1);

bool recognition(Mat& img, int x);

int Max_rect_value;
String Slist[31] = { "藏","川","鄂","甘","赣","贵","桂","黑","沪","吉","冀","津","晋","京","辽","鲁","蒙","闽","宁","青","琼","陕","苏","皖","湘","新","渝","豫","粤","云","浙" };
String EngList[24] = { "A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z" };
String Mix[34] = { "0","1","2","3","4","5","6","7","8","9" ,"A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z" };
int main(int argc, char *argv[])
{
	Mat src = imread("car6.jpg");
	cout << src.cols << "," << src.rows << endl;
	Mat dst;
	Mat temp;
	Mat ReImage;

	Mat c_src = src.clone();
	blur(src, dst, Size(7, 7));
	IplImage *mid;
	if (src.empty())return -1;

	mid = pic_Thresholding(&IplImage(dst));
	temp = Mat(mid);
	//imshow("二值化", temp);
	Canny(temp, temp, 50, 150, 3);
	//namedWindow("result");
	//imshow("result", temp);

	std::vector<cv::Vec4i> lines;//直线数组
	cv::HoughLinesP(temp, lines, 1, CV_PI / 180, 20, 20, 40);
	cv::Point2f center(0, 0);//参考点
	cv::Vec4f final_lines[4];
	if (lines.size() >= 4) {//找到最左、上、右、下得四条边
		int flag = 0;
		int left = src.cols;
		int top = src.rows;
		int right = 0;
		int bottom = 0;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < lines.size(); j++) {
				cv::Vec4i t = lines[j];
				if (flag == 0) {
					if ((t[0] + t[2]) / 2 < left) {
						left = (t[0] + t[2]) / 2;
						final_lines[i] = lines[j];
					}
				}
				if (flag == 1) {
					if ((t[1] + t[3]) / 2  < top) {
						top = (t[1] + t[3]) / 2;
						if (top>0)
							final_lines[i] = lines[j];
					}
				}
				if (flag == 2) {
					if ((t[0] + t[2]) / 2  > right) {
						right = (t[0] + t[2]) / 2;
						final_lines[i] = lines[j];
					}
				}
				if (flag == 3) {
					if ((t[1] + t[3]) / 2  > bottom) {
						bottom = (t[1] + t[3]) / 2;
						final_lines[i] = lines[j];
					}
				}
			}
			flag++;
		}
		//---------------------------------
		for (int i = 0; i < 4; i++)// Expand the lines
		{
			cv::Vec4i v = final_lines[i];
			if (abs(v[0] - v[2]) >= 2) {
				final_lines[i][0] = 0;
				final_lines[i][1] = (float)(v[1] - v[3]) / (v[0] - v[2]) * -v[0] + v[1];

				final_lines[i][2] = src.cols;
				final_lines[i][3] = (float)(v[1] - v[3]) / (v[0] - v[2]) * (src.cols - v[2]) + v[3];
			}
			else {
				final_lines[i][1] = 0;
				final_lines[i][3] = src.rows;
			}
		}
		for (int i = 0; i < 4; i++)
		{
			cv::Vec4i v = final_lines[i];
			cv::line(src, cv::Point(v[0], v[1]), cv::Point(v[2], v[3]), CV_RGB(0, 255, 0));
		}
		std :: vector<cv::Point2f> corners;//顶点

		for (int i = 0; i < 4; i++)
		{
			cv::Point2f pt = getPoint(final_lines[i], final_lines[(i + 1) % 4]);
			corners.push_back(pt);
		}
		std::vector<cv::Point2f> approx;//多边形
		cv::approxPolyDP(cv::Mat(corners), approx, cv::arcLength(cv::Mat(corners), true) * 0.02, true);//多边形拟合
		cout << approx.size() << endl;

		if (approx.size() != 4)
		{
			std::cout << "The object is not quadrilateral!" << std::endl;
			return -1;
		}

		// Get mass center
		for (int i = 0; i < corners.size(); i++)
			center += corners[i];
		center *= (1. / corners.size());

		sortCorners(corners, center);
		if (corners.size() == 0) {
			std::cout << "The corners were not sorted correctly!" << std::endl;
			return -1;
		}
		// Draw corner points
		cv::circle(src, corners[0], 3, CV_RGB(255, 0, 0), 2);
		cv::circle(src, corners[1], 3, CV_RGB(0, 255, 0), 2);
		cv::circle(src, corners[2], 3, CV_RGB(0, 0, 255), 2);
		cv::circle(src, corners[3], 3, CV_RGB(255, 255, 255), 2);
		// Draw mass center
		cv::circle(src, center, 3, CV_RGB(255, 255, 0), 2);

		cv::Mat quad = cv::Mat::zeros(140, 440, CV_8UC3);
		std::vector<cv::Point2f> quad_pts;
		quad_pts.push_back(cv::Point2f(0, 0));
		quad_pts.push_back(cv::Point2f(quad.cols, 0));
		quad_pts.push_back(cv::Point2f(quad.cols, quad.rows));
		quad_pts.push_back(cv::Point2f(0, quad.rows));

		cv::Mat transmtx = cv::getPerspectiveTransform(corners, quad_pts);
		cv::warpPerspective(c_src, quad, transmtx, quad.size());
		cv::imshow("image", src);
		cv::imshow("quadrilateral", quad);
		Mat grayImg;
		cvtColor(quad, grayImg, CV_BGR2GRAY);
		threshold(grayImg, grayImg, 150, 255, CV_THRESH_OTSU);
		int t_x, t_x2;
		Mat detection;
		Mat newrect;
		//上下裁剪
		detectionChange(grayImg, detection, 7, quad, newrect);

		int *vArry = new int[detection.cols];
		//计算有效区域
		verProjection_calculate(detection, vArry, detection.cols);

		//for (int i = 0; i < detection.cols - 1; i++)cout << vArry[i];
		cout <<detection.rows<<" "<<detection.cols<< endl;
		int **pic_Arr,pic_ArryNumber,pic_width;
		//分割有效区域
		pic_Arr = verProjection_cut(vArry, detection.cols, &pic_ArryNumber);

		cout << pic_ArryNumber << endl;
		//显示分割字符
		for (int i = 0; i < pic_ArryNumber; i++) {
			pic_width = pic_Arr[i][1] - pic_Arr[i][0];
			if (pic_width < 5) {
				continue;
			}
			Mat img_1 = detection(Rect(pic_Arr[i][0],0,pic_Arr[i][1] - pic_Arr[i][0], detection.rows));
			
			imshow("res"+to_string(i), img_1);
			
		}
		vector<int> compression_params;
		compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);  //选择jpeg
		compression_params.push_back(100); //在这个填入你要的图片质量
		rectangle(newrect, Rect(6, 0, 50, 100), CV_RGB(255, 0, 0), 1);
		rectangle(newrect, Rect(65, 0, 49, 100), CV_RGB(255, 0, 0), 1);
		rectangle(newrect, Rect(147, 0, 50, 100), CV_RGB(255, 0, 0), 1);
		rectangle(newrect, Rect(206, 0, 50, 100), CV_RGB(255, 0, 0), 1);
		rectangle(newrect, Rect(266, 0, 50, 100), CV_RGB(255, 0, 0), 1);
		rectangle(newrect, Rect(327, 0, 50, 100), CV_RGB(255, 0, 0), 1);
		rectangle(newrect, Rect(387, 0, 50, 100), CV_RGB(255, 0, 0), 1);
		cout << "车牌号码为:";
		Mat ImgROI = detection(Rect(6, 0, 50, 100));

		recognition(ImgROI, 1);
		//imwrite("output/1.jpg", ImgROI);
		ImgROI = detection(Rect(65, 0, 49, 100));
		recognition(ImgROI, 2);
		//imwrite("output/2.jpg", ImgROI);
		ImgROI = detection(Rect(147, 0, 50, 100));
		recognition(ImgROI, 3);
		//imwrite("output/3.jpg", ImgROI);
		ImgROI = detection(Rect(206, 0, 50, 100));
		recognition(ImgROI, 4);
		//imwrite("output/4.jpg", ImgROI);
		ImgROI = detection(Rect(266, 0, 50, 100));
		recognition(ImgROI, 5);
		//imwrite("output/5.jpg", ImgROI);
		ImgROI = detection(Rect(327, 0, 50, 100));
		recognition(ImgROI, 6);
		//imwrite("output/6.jpg", ImgROI);

		ImgROI = detection(Rect(388, 0, 50, 100));
		recognition(ImgROI, 7);
		//imwrite("output/7.jpg", ImgROI,compression_params);
		if (!detection.empty())
			imshow("效果图", newrect);
		cv::waitKey(0);
	}
	else return -1;

	return 0;
}
//--------------------------------------------
IplImage* pic_Thresholding(IplImage *img) {
	CvScalar s;
	IplImage *temp = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
	int width = img->width;
	int height = img->height;
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			s = cvGet2D(img, j, i);
			if (s.val[0] > 140 && s.val[1] < 120 && s.val[1]>80 && s.val[2] <50 && s.val[2]>5)
			{
				s.val[0] = 255;
				s.val[1] = 255;
				s.val[2] = 255;
			}
			else {
				s.val[0] = 0;
				s.val[1] = 0;
				s.val[2] = 0;
			}
			cvSet2D(temp, j, i, s);
		}
	}
	return temp;
}
int detectionChange(Mat & mat1, Mat & mat2, int number, Mat& mat3, Mat& mat4)
{
	IplImage pl_1 = mat1, pl_2;
	CvScalar s1, s2;
	int width = mat1.rows;
	int height = mat1.cols;
	int sum = 0, sum_2 = 0, width_1 = 0, width_2 = 0;
	int i, j, height_1;
	for (i = 0; i < width; i++) {
		sum = 0;
		sum_2 = 0;
		for (j = 0; j < height - 1; j++) {
			s1 = cvGet2D(&pl_1, i, j);
			s2 = cvGet2D(&pl_1, i, j + 1);
			if (((int)s1.val[0]) != ((int)s2.val[0])) {
				sum += 1;
				sum_2 = 0;
			}
			else {
				sum_2 += 1;
			}
			if (sum_2 != 0) {
				if (height / sum_2 < 5) {
					sum = 0;
					break;
				}
			}

		}
		if (sum >= number) {
			width_1 = i;
			break;

		}
		else {
			width_1 = i;
		}

	}
	for (i = width - 1; i >0; i--) {
		sum = 0;
		sum_2 = 0;
		for (j = 0; j < height - 1; j++) {
			s1 = cvGet2D(&pl_1, i, j);
			s2 = cvGet2D(&pl_1, i, j + 1);

			if (((int)s1.val[0]) != ((int)s2.val[0])) {
				sum += 1;
				sum_2 = 0;
			}
			else {
				sum_2 += 1;
			}
			if (sum_2 != 0) {
				if (height / sum_2 < 1) {
					sum = 0;
					break;
				}
			}
		}
		if (sum >= number) {
			width_2 = i;
			break;

		}
		else {
			width_2 = i;
		}
	}
	if (width_2 <= width_1) {
		width_2 = width;
	}
	mat2 = Mat(width_2 - width_1 + 1, height, CV_8UC1, 1);
	mat4 = mat3(Rect(0, width_1, height, width_2 - width_1));
	pl_2 = mat2;
	for (i = width_1; i < width_2; i++) {
		for (j = 0; j < height; j++) {
			s1 = cvGet2D(&pl_1, i, j);
			cvSet2D(&pl_2, i - width_1, j, s1);
		}


	}
	return 0;
}
void verProjection_calculate(Mat & mat1, int * vArry, int number)
{

	IplImage pl_1 = mat1;
	CvScalar s1;
	int width = mat1.rows;
	int height = mat1.cols;
	int i, j;
	for (i = 0; i < number; i++) {
		vArry[i] = 0;
	}
	for (j = 0; j < height; j++) {
		for (i = 0; i < width; i++) {
			s1 = cvGet2D(&pl_1, i, j);
			if (s1.val[0] > 20) {
				
				vArry[j] += 1;
			}
		}
	}

}
int ** verProjection_cut(int * vArr, int width, int * number)
{
	int**a;
	int i, flag = 0;
	int num = 0;
	int threshold = 2;
	a = (int**)malloc(width / 2 * sizeof(int *));
	for (i = 0; i<width - 1; i++) {
		if ((vArr[i] <= threshold) && (vArr[i + 1] > threshold)) {
			a[num] = (int*)malloc(2 * sizeof(int));
			a[num][0] = i;
			flag = 1;
		}
		else if ((vArr[i] > threshold) && (vArr[i + 1] <= threshold) && (flag != 0)) {
			a[num][1] = i;
			num += 1;
			flag = 0;
		}
	}
	*number = num;
	return a;
}
float pixelPerectange(Mat & mat1)
{
	IplImage pl_1 = mat1;
	CvScalar s1;
	int width = mat1.rows;
	int height = mat1.cols;
	int i, j;
	float sum = 0, allSum = 0, tmp;
	for (i = 0; i < width; i++) {
		for (j = 0; j < height; j++) {
			s1 = cvGet2D(&pl_1, i, j);
			if (s1.val[0] > 20) {
				sum += 1;
			}
			allSum += 1;
		}
	}
	tmp = sum / allSum;
	return tmp;
}
//-------------------------------------------
cv::Point2f getPoint(cv::Vec4i a, cv::Vec4i b) {
	int x1 = a[0], y1 = a[1], x2 = a[2], y2 = a[3], x3 = b[0], y3 = b[1], x4 = b[2], y4 = b[3];
	double k1, k2;
	cv::Point2f pt;
	//斜率存在
	if (x1 - x2 != 0 && x3 - x4 != 0) {
		k1 = (float)(y1 - y2) / (x1 - x2);
		k2 = (float)(y3 - y4) / (x3 - x4);

		pt.x = (k1 * x1 - k2 * x3 + y3 - y1) / (k1 - k2);

		pt.y = k1*(pt.x - x1) + y1;
	}
	//斜率不存在
	else {
		if (x1 - x2 == 0) {
			k1 = (double)(y3 - y4) / (x3 - x4);
			pt.x = x1;
			pt.y = k1*(pt.x - x3) + y3;
		}
		else {
			k2 = (double)(y1 - y2) / (x1 - x2);
			pt.x = x3;
			pt.y = k2*(pt.x - x1) + y1;
		}
	}
	return pt;

}
//-------------------------------------------
void sortCorners(std::vector<cv::Point2f>& corners, cv::Point2f center) //顶点排序
{
	std::vector<cv::Point2f> top, bot;
	for (int i = 0; i < corners.size(); i++)
	{
		if (corners[i].y < center.y)
			top.push_back(corners[i]);
		else
			bot.push_back(corners[i]);
	}
	corners.clear();
	if (top.size() == 2 && bot.size() == 2) {
		cv::Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
		cv::Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
		cv::Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
		cv::Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];
		corners.push_back(tl);
		corners.push_back(tr);
		corners.push_back(br);
		corners.push_back(bl);
	}
}

bool recognition(Mat& img, int x) {
	Mat temp;

	int rate = 0;
	String path;
	int n;
	switch (x)
	{
	case 1:
		path = "output/Chinese/";
		n = 31;
		break;
	case 2:
		path = "output/English/";
		n = 24;
		break;

	default:
		path = "output/Mix/";
		n = 34;
		break;

	}

	cv::resize(img, img, cv::Size(20, 40), (0, 0), (0, 0), cv::INTER_LINEAR);
	int rownumber = img.rows;
	int colnumber = img.cols*img.channels();
	String link;
	for (int k = 0; k < n - 1; k++) {
		rate = 0;

		if (x == 1) {
			link = path + Slist[k] + ".bmp";
		}
		else if (x == 2) {
			link = path + EngList[k] + ".bmp";
		}
		else {
			link = path + Mix[k] + ".bmp";
		}
		temp = imread(link);
		cvtColor(temp, temp, CV_BGR2GRAY);
		threshold(temp, temp, 150, 255, CV_THRESH_OTSU);
		//imshow("temp",temp);
		//imshow("img", img);
		//cvWaitKey(0);
		for (int i = 0; i < rownumber; i++) {
			char* data1 = img.ptr<char>(i);
			char* data2 = temp.ptr<char>(i);

			for (int j = 0; j < colnumber; j++) {

				if (data1[j] ^ data2[j] == 0)rate++;

			}
		}
		if ((float)rate / (colnumber*rownumber) >0.95f) {

			if (x == 1)cout << Slist[k];
			else if (x == 2)cout << EngList[k];
			else if (x == 7)cout << Mix[k] << endl;
			else cout << Mix[k];
			return true;
		}
		temp.release();
	}


	return false;

}