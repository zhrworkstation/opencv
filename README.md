# OpenCV车牌识别测试版

#### 介绍

1. 原图片
![原图片](https://images.gitee.com/uploads/images/2019/0304/232109_c1dfd30f_1199334.jpeg "car6.jpg")
2. 车牌定位
![车牌定位](https://images.gitee.com/uploads/images/2019/0304/232154_026c6df6_1199334.png "QQ截图20190304231540.png")
3. 透视变换
![透视变换](https://images.gitee.com/uploads/images/2019/0304/232214_5af8bde2_1199334.png "QQ截图20190304231644.png")
4. 文字定位
![文字定位](https://images.gitee.com/uploads/images/2019/0304/232249_0d18378d_1199334.png "QQ截图20190304231657.png")
5. 文字定位以后进行模板匹配即可得出车牌号
![输入图片说明](https://images.gitee.com/uploads/images/2019/0304/232343_2f366aa7_1199334.png "QQ截图20190304231723.png")


#### 软件架构
软件架构说明


#### 安装教程

1. 安装opencv 2.4.13
2. 下载CMake生成对应编译器的dll
3. 配置环境变量以及opencv头文件以及lib目录


#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)